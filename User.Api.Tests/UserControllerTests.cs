using FluentAssertions;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using User.Api.Controllers;
using User.Api.Data;
using User.Api.Model;
using Xunit;

namespace User.Api.Tests
{
    public class UserControllerTests
    {
        private UserContext GetUserContext()
        {
            var options = new DbContextOptionsBuilder<UserContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString()).Options;

            var userContext = new UserContext(options);

            userContext.Users.Add(new AppUser { Id = 1, Name = "davon", ProvinceId = 0, CityId = 1 });

            userContext.SaveChanges();

            return userContext;
        }

        private (UserController, UserContext) InitUserController()
        {
            var userContext = GetUserContext();

            var loggerMoq = new Mock<ILogger<UserController>>();
            var logger = loggerMoq.Object;

            var controller = new UserController(userContext, logger);

            return (controller, userContext);
        }

        [Fact]
        public async Task Get_ReturnRightUser_WithExpectedParameters()
        {
            (var controller, var userContext) = InitUserController();

            var response = await controller.Get(1);

            var result = response.Should().BeOfType<OkObjectResult>().Subject;

            var appUser = result.Value.Should().BeAssignableTo<AppUser>().Subject;

            appUser.Id.Should().Be(1);
            appUser.Name.Should().Be("davon");
        }

        [Fact]
        public async Task Patch_ReturnNewName_WithExpectedNewPropertiesName()
        {
            (var controller, var userContext) = InitUserController();

            var document = new JsonPatchDocument<AppUser>();
            document.Add(m => m.Name, "libruners");

            var response = await controller.Patch(document);
            var result = response.Should().BeOfType<OkObjectResult>().Subject;

            // assert response
            var appUser = result.Value.Should().BeAssignableTo<AppUser>().Subject;
            appUser.Name.Should().Be("libruners");

            // assert name value in ef context
            appUser = await userContext.Users.FindAsync(1);
            appUser.Should().NotBeNull();
            appUser.Name.Should().Be("libruners");
        }

        [Fact]
        public async Task Patch_ReturnNewParameters_WithExpectedNewParameters()
        {
            (var controller, var userContext) = InitUserController();

            var document = new JsonPatchDocument<AppUser>();
            document.Add(m => m.Properties, new List<UserProperty>()
            {
                new UserProperty{ Key = "fin_industry", Value = "互联网", Text = "互联网"},
                new UserProperty{ Key = "fin_industry", Value = "物联网", Text = "物联网"},
            });

            var response = await controller.Patch(document);
            var result = response.Should().BeOfType<OkObjectResult>().Subject;

            // assert response
            var appUser = result.Value.Should().BeAssignableTo<AppUser>().Subject;
            appUser.Properties.Count.Should().Be(2);
            appUser.Properties.First().Value.Should().Be("互联网");
            appUser.Properties.First().Key.Should().Be("fin_industry");
            appUser.Properties.Last().Value.Should().Be("物联网");
            appUser.Properties.Last().Key.Should().Be("fin_industry");

            // assert name value in ef context
            appUser = await userContext.Users.FindAsync(1);
            appUser.Properties.Count.Should().Be(2);
            appUser.Properties.First().Value.Should().Be("互联网");
            appUser.Properties.First().Key.Should().Be("fin_industry");
            appUser.Properties.Last().Value.Should().Be("物联网");
            appUser.Properties.Last().Key.Should().Be("fin_industry");
        }

        [Fact]
        public async Task Patch_ReturnRemoveParameters_WithExpectedRemoveParameters()
        {
            (var controller, var userContext) = InitUserController();

            var document = new JsonPatchDocument<AppUser>();
            document.Add(m => m.Properties, new List<UserProperty>() { });

            var response = await controller.Patch(document);
            var result = response.Should().BeOfType<OkObjectResult>().Subject;

            // assert response
            var appUser = result.Value.Should().BeAssignableTo<AppUser>().Subject;
            appUser.Properties.Should().BeEmpty();

            // assert name value in ef context
            appUser = await userContext.Users.FindAsync(1);
            appUser.Properties.Should().BeEmpty();
        }
    }
}