﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using User.Api.Data;
using Microsoft.EntityFrameworkCore;
using User.Api.Model;
using System.Diagnostics;
using System.Threading;
using User.Api.Filter;

namespace User.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<UserContext>(options =>
            {
                options.UseMySQL(Configuration.GetConnectionString("MysqlUser"));
            });

            services.AddMvc(options =>
            {
                options.Filters.Add(typeof(GlobalExceptionFilter));
            }).SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();

            //SeedAsync(app).Wait();
        }

        //public async Task SeedAsync(IApplicationBuilder app)
        //{
        //    Stopwatch stopwatch = new Stopwatch();
        //    stopwatch.Start();

        //    using (var scope = app.ApplicationServices.CreateScope())
        //    {
        //        while (stopwatch.Elapsed.Seconds <= 360)
        //        {
        //            try
        //            {
        //                var userContext = scope.ServiceProvider.GetRequiredService<UserContext>();

        //                userContext.Database.Migrate();

        //                var count = userContext.Users.ToList().Count;
        //                if (count == 0)
        //                {
        //                    userContext.Users.Add(new AppUser { Name = "davon", Company = "huantai" });

        //                    userContext.SaveChanges();
        //                }

        //                return;
        //            }
        //            catch (Exception ex)
        //            {
        //                Thread.Sleep(5000);
        //                Console.WriteLine(ex.Message);
        //            }
        //        }
        //    }
        //}
    }
}
