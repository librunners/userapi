﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace User.Api.Filter
{
    public class GlobalExceptionFilter : IExceptionFilter
    {
        private readonly IHostingEnvironment _env;
        private readonly ILogger<GlobalExceptionFilter> _logger;

        public GlobalExceptionFilter(IHostingEnvironment env, ILogger<GlobalExceptionFilter> logger)
        {
            _env = env;
            _logger = logger;
        }

        public void OnException(ExceptionContext context)
        {
            var resultJson = new JsonErrorResponse();

            if (context.Exception.GetType() == typeof(UserOperationException))
            {
                resultJson.Message = context.Exception.Message;

                context.Result = new BadRequestObjectResult(resultJson);
            }
            else
            {
                resultJson.Message = "发生了未知的内部错误";

                if (_env.IsDevelopment())
                    resultJson.DeveloperMessage = context.Exception.StackTrace;

                context.Result = new InternalServerErrorObjectError(resultJson);
            }

            _logger.LogError(context.Exception, context.Exception.Message);

            context.ExceptionHandled = true;
        }
    }

    public class InternalServerErrorObjectError : ObjectResult
    {
        public InternalServerErrorObjectError(object value) : base(value)
        {
            StatusCode = StatusCodes.Status500InternalServerError;
        }
    }
}
