﻿namespace User.Api.Model
{
    /// <summary>
    /// 用户属性
    /// </summary>
    public class UserProperty
    {
        private int? _requestedHashCode;

        /// <summary>
        /// 用户id
        /// </summary>
        public int AppUserId { get; set; }

        /// <summary>
        /// 键
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// 值
        /// </summary>
        public string Value { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is UserProperty))
            {
                return false;
            }
            if (object.ReferenceEquals(this, obj))
                return true;
            UserProperty item = (UserProperty)obj;
            if (item.IsTransient() || this.IsTransient())
                return false;
            else
            {
                return item.Key == this.Key && item.Value == this.Value;
            }
        }

        public override int GetHashCode()
        {
            if (!IsTransient())
            {
                if (!_requestedHashCode.HasValue)
                {
                    _requestedHashCode = (this.Key + this.Value).GetHashCode() ^ 31;
                }
                return _requestedHashCode.Value;
            }
            return base.GetHashCode();
        }

        public bool IsTransient()
        {
            return string.IsNullOrEmpty(this.Key) || string.IsNullOrEmpty(this.Value);
        }
    }
}