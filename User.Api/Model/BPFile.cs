﻿using System;

namespace User.Api.Model
{
    public class BPFile
    {
        /// <summary>
        /// 主键
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 上传用户id
        /// </summary>
        public int AppUserId { get; set; }

        /// <summary>
        /// 文件名称
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// 原始文件路径
        /// </summary>
        public string OriginFilePath { get; set; }

        /// <summary>
        /// 文件路径
        /// </summary>
        public string FormatFilePath { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreatedTime { get; set; }
    }
}