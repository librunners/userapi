﻿using System.Collections.Generic;

namespace User.Api.Model
{
    /// <summary>
    /// 用户信息
    /// </summary>
    public class AppUser
    {
        public AppUser()
        {
            Properties = new List<UserProperty>();
        }

        /// <summary>
        /// 主键
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 用户名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 公司名
        /// </summary>
        public string Company { get; set; }

        /// <summary>
        /// 公司职位
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// 头像地址
        /// </summary>
        public string Avatar { get; set; }

        /// <summary>
        /// 性别 1：男， 0：女
        /// </summary>
        public byte Gender { get; set; }

        /// <summary>
        /// 详细地址
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// 电子邮件
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// 电话
        /// </summary>
        public string Tel { get; set; }

        /// <summary>
        /// 省份id
        /// </summary>
        public int ProvinceId { get; set; }

        /// <summary>
        /// 省份名称
        /// </summary>
        public string Province { get; set; }

        /// <summary>
        /// 城市id
        /// </summary>
        public int CityId { get; set; }

        /// <summary>
        /// 城市名称
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// 会员名片
        /// </summary>
        public string MemberCard { get; set; }

        /// <summary>
        /// 用户属性
        /// </summary>
        public List<UserProperty> Properties { get; set; }
    }
}