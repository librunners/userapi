﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using User.Api.Dtos;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace User.Api.Controllers
{
    public class BaseController : ControllerBase
    {
        protected UserIdentity UserIdentity = new UserIdentity
        {
            UserId = 1,
            UserName = "davon",
            Avatar = "https://cdn.duitang.com/uploads/item/201411/12/20141112002035_zaJha.jpeg"
        };
    }
}
