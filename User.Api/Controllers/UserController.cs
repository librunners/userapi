﻿using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using User.Api.Data;
using User.Api.Model;

namespace User.Api.Controllers
{
    [Route("api/users")]
    [ApiController]
    public class UserController : BaseController
    {
        private readonly UserContext _userContext;
        private readonly ILogger<UserController> _logger;

        public UserController(UserContext userContext, ILogger<UserController> logger)
        {
            _userContext = userContext;
            _logger = logger;
        }

        // GET api/users
        [HttpGet]
        public async Task<IActionResult> Get(int id)
        {
            var model = await _userContext.Users
                .AsNoTracking()
                .Include(x => x.Properties)
                .SingleOrDefaultAsync(x => x.Id == UserIdentity.UserId);

            if (model == null)
                throw new UserOperationException("该用户id不存在");

            return Ok(model);
        }

        // PATCH api/users
        [HttpPatch]
        public async Task<IActionResult> Patch([FromBody]JsonPatchDocument<AppUser> patch)
        {
            var user = await _userContext.FindAsync<AppUser>(UserIdentity.UserId);
            if (user == null)
                throw new UserOperationException($"错误的用户上下文Id:{UserIdentity.UserId}");

            patch.ApplyTo(user);

            var currentProps = user.Properties;
            var originProps = await _userContext.UserProperties
                .AsNoTracking()
                .Where(m => m.AppUserId == UserIdentity.UserId)
                .ToListAsync();

            var allProps = originProps.Union(user.Properties).Distinct();

            var removeProps = originProps.Except(user.Properties);
            var insertProps = allProps.Except(originProps);

            foreach (var item in user?.Properties)
            {
                _userContext.Entry(item).State = EntityState.Detached;
            }

            foreach (var item in removeProps)
            {
                _userContext.Remove(item);
            }

            foreach (var item in insertProps)
            {
                await _userContext.AddAsync(item);
            }

            _userContext.Users.Update(user);
            await _userContext.SaveChangesAsync();
            return Ok(user);
        }
    }
}