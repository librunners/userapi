﻿using Microsoft.EntityFrameworkCore;
using User.Api.Model;

namespace User.Api.Data
{
    public class UserContext : DbContext
    {
        public UserContext(DbContextOptions<UserContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AppUser>().ToTable("Users").HasKey(m => m.Id);

            modelBuilder.Entity<UserProperty>().ToTable("UserProperties").HasKey(x => new { x.AppUserId, x.Key, x.Value });
            modelBuilder.Entity<UserProperty>().Property(x => x.Key).HasMaxLength(100);
            modelBuilder.Entity<UserProperty>().Property(x => x.Value).HasMaxLength(100);

            modelBuilder.Entity<UserTag>().ToTable("UserTags").HasKey(x => new { x.AppUserId, x.Tag });
            modelBuilder.Entity<UserTag>().Property(x => x.Tag).HasMaxLength(100);
            modelBuilder.Entity<UserTag>().Property(x => x.CreatedTime).ValueGeneratedOnAdd();

            modelBuilder.Entity<BPFile>().ToTable("BPFiles").HasKey(m => m.Id);
            modelBuilder.Entity<BPFile>().Property(x => x.CreatedTime).ValueGeneratedOnAdd();

            base.OnModelCreating(modelBuilder);
        }

        public DbSet<AppUser> Users { get; set; }

        public DbSet<UserTag> UserTags { get; set; }

        public DbSet<UserProperty> UserProperties { get; set; }

        public DbSet<BPFile> BPFiles { get; set; }
    }
}